/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.inheritance;

/**
 *
 * @author MSI GAMING
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Dog mome = new Dog("Mome","Black&White");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat","Black&White");
        bat.speak();
        bat.walk();
        
        Dog to = new Dog("To","Orange&White");
        to.speak();
        to.walk();
        
        Duck gabgab = new Duck("GabGab","Black");
        gabgab.speak();
        gabgab.walk();
        
        
        System.out.println("Dang is Animal: "+ (dang instanceof Animal));
        System.out.println("Dang is Duck: "+ (dang instanceof Object));
        System.out.println("Dang is Dog: "+ (dang instanceof Dog));
        System.out.println("Dang is Cat: "+ (dang instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("Zero is Animal: "+ (zero instanceof Animal));
        System.out.println("Zero is Duck: "+ (zero instanceof Object));
        System.out.println("Zero is Dog: "+ (zero instanceof Object));
        System.out.println("Zero is Cat: "+ (zero instanceof Cat));
        System.out.println("Animal is Cat: "+ (animal instanceof Cat));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Duck: "+ (zom instanceof Duck));
        System.out.println("Zom is Dog: "+ (zom instanceof Object));
        System.out.println("Zom is Cat: "+ (zom instanceof Object));
        System.out.println("Animal is Duck: "+ (animal instanceof Duck));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("Mome is Animal: "+ (mome instanceof Animal));
        System.out.println("Mome is Duck: "+ (mome instanceof Object));
        System.out.println("Mome is Dog: "+ (mome instanceof Dog));
        System.out.println("Mome is Cat: "+ (mome instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("Bat is Animal: "+ (bat instanceof Animal));
        System.out.println("Bat is Duck: "+ (bat instanceof Object));
        System.out.println("Bat is Dog: "+ (bat instanceof Dog));
        System.out.println("Bat is Cat: "+ (bat instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("To is Animal: "+ (to instanceof Animal));
        System.out.println("To is Duck: "+ (to instanceof Object));
        System.out.println("To is Dog: "+ (to instanceof Dog));
        System.out.println("To is Cat: "+ (to instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        System.out.println("Gabgab is Animal: "+ (gabgab instanceof Animal));
        System.out.println("Gabgab is Duck: "+ (gabgab instanceof Duck));
        System.out.println("Gabgab is Dog: "+ (gabgab instanceof Object));
        System.out.println("Gabgab is Cat: "+ (gabgab instanceof Object));
        System.out.println("Animal is Duck: "+ (animal instanceof Duck));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        
        Animal[] animals = {dang,zero,zom,mome,bat,to,gabgab};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
        
    }
}
